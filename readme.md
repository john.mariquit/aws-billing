# Ingesting AWS Billing Data without RedShift/Athena

This is an alternative approach to using AWS RedShift or AWS Athena to ingest your AWS Billing Data.

## Sample Use Case
Finance department from time-to-time needs to get more details out of the monthly AWS Invoice they receive.

## Pre-requisites

- Consolidated Billing should have been enabled on the AWS master account
- AWS CLI v2
